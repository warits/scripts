#!/bin/bash

cd $1
git init .
git add -A
git commit -m "init rep"
git remote add bitbucket ssh://git@bitbucket.org/warits/work-$1.git
git push -u bitbucket --all
cd -

