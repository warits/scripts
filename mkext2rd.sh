#!/bin/bash

dd if=/dev/zero of=./tmp1 bs=4K count=128
mke2fs -F -m0 tmp1
sudo mkdir /mnt/tmpext
sudo mount -t ext2 -o loop tmp1 /mnt/tmpext
sudo cp $1/* /mnt/tmpext -pdrf
sudo umount /mnt/tmpext
sudo rm -rf /mnt/tmpext
gzip -9 tmp1
mkimage -A arm -O linux -T kernel -C none -a 0x41000000 -n "RAMDisk" -d tmp1.gz ramdisk.ext2
rm tmp1.gz
